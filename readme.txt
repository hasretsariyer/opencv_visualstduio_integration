1. Download OpenCV 2.4.8 at http://opencv.org/downloads.html

2. Create a Win32 console application

3. Config opencv..
3.1. Menu Project ---- $ProjectName Properties

3.2. Choose Configuration Manager... and add x64 platform

3.3. At configuration field, choose All configuration
3.3.1. At Configuration Properties ---- C/C++ ---- Additional Include Directories, add opencv include folders
$opencv\build\include
$opencv\build\opencv
$opencv\build\opencv2 NOTE: $opencv is a folder that you have installed opencv

3.3.2. At Configuration Properties ---- Linker ---- Additional Library Directories, add opencv library folder
$opencv\build\x64\vc11\lib

3.4. At configuration field, choose Debug mode
3.4.1. At Configuration Properties ---- Linker ---- Additional dependencies ---- add dependence libraries
opencv_stitching248d.lib
opencv_contrib248d.lib
opencv_videostab248d.lib
opencv_superres248d.lib
opencv_nonfree248d.lib
opencv_gpu248d.lib
opencv_ocl248d.lib
opencv_legacy248d.lib
opencv_ts248d.lib
opencv_calib3d248d.lib
opencv_features2d248d.lib
opencv_objdetect248d.lib
opencv_highgui248d.lib
opencv_video248d.lib
opencv_photo248d.lib
opencv_imgproc248d.lib
opencv_flann248d.lib
opencv_ml248d.lib
opencv_core248d.lib
NOTE: These file above are located at .\opencv\build\x64\vc11\lib\

3.5. At configuration field, choose Release mode
3.5.1. At Configuration Properties ---- Linker ---- Additional dependencies ---- add dependence libraries
opencv_stitching248.lib
opencv_contrib248.lib
opencv_videostab248.lib
opencv_superres248.lib
opencv_nonfree248.lib
opencv_gpu248.lib
opencv_ocl248.lib
opencv_legacy248.lib
opencv_ts248.lib
opencv_calib3d248.lib
opencv_features2d248.lib
opencv_objdetect248.lib
opencv_highgui248.lib
opencv_video248.lib
opencv_photo248.lib
opencv_imgproc248.lib
opencv_flann248.lib
opencv_ml248.lib
opencv_core248.lib
NOTE: These file above are located at .\opencv\build\x64\vc11\lib\

3.6. Add $opencv\build\x64\v11\bin to System Environment Path
3.7. Restart Visual studio